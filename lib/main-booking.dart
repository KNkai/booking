import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:sentry/sentry.dart';
import 'package:super_init_app/routes.dart';
import 'base_config/base_config.dart';
import 'package:timeago/timeago.dart' as timeago;

GlobalKey appKey = GlobalKey();

void main() async {
  runZoned(() async {
    timeago.setLocaleMessages('vi', timeago.ViMessages());
    runApp(MyApp());
  }, onError: _reportError);
  //FirebaseService.instance.init();
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      return OrientationBuilder(builder: (context, orientation) {
        SizeConfig().init(constraints, orientation);
        return Provider(
          create: (_) => AppBloc(),
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              GlobalCupertinoLocalizations.delegate,
            ],
            supportedLocales: [
              const Locale('en'), // English
              const Locale('vi'), // Hebrew
            ],
            theme: ThemeData(
              primaryColor: Colors.white,
              scaffoldBackgroundColor: HexColor("#FFFFFF"),
              fontFamily: 'Quicksand',
            ),
            builder: (context, child) {
              return RepaintBoundary(
                key: appKey,
                child: MediaQuery(
                  child: child,
                  data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
                ),
              );
            },
            navigatorKey: navigatorKey,
            onGenerateRoute: onGenerateRoute,
            initialRoute: RoutePaths.bookingDashboard,
          ),
        );
      });
    });
  }
}

final SentryClient _sentry = SentryClient(
    dsn:
        "https://817097a11b6c454dbb8f095554c342e6@o396604.ingest.sentry.io/5262309");

Future<Null> _reportError(dynamic error, dynamic stackTrace) async {
  final SentryResponse response = await _sentry.captureException(
    exception: error,
    stackTrace: stackTrace,
  );

  if (response.isSuccessful) {
    print('Success! Event ID: ${response.eventId}');
  } else {
    print('Failed to report to Sentry.io: ${response.error}');
  }
}
