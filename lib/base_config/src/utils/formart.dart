import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

class Formart {
  static toVNDCurency(int value) {
    final formatter = NumberFormat.currency(locale: "vi_VN");
    String newValue = formatter.format(value);
    return newValue;
  }

  static toVNDCurency2(int value) {
    final formatter = NumberFormat.currency(locale: "vi_VN");
    String newValue = formatter.format(value);
    newValue = newValue.substring(0, newValue.length-3);
    newValue = newValue + 'đ';
    return newValue;
  }

  String formatTime(String time) {
    var result = timeago.format(DateTime.parse(time).toLocal(), locale: 'vi');
    if (result == "một thoáng trước") {
      return "khoảng một phút trước";
    }

    return result;
  }

  String formatToDateTime(String time) {
    var result = DateTime.parse(time);
    return '${result.day}/${result.month}/${result.year}';
  }

  static String formatErrFirebaseLoginToString(String err) {
    String message = "";
    switch (err) {
      case "ERROR_ARGUMENT_ERROR":
        message = "Vui lòng nhập đầy đủ dữ liệu";
        break;
      case "ERROR_OPERATION_NOT_ALLOWED":
        message = "Phương thức đăng nhập này chưa được cho phép";
        break;
      case "ERROR_USER_DISABLED":
        message = "Tài khoản này đã bị khoá";
        break;
      case "ERROR_INVALID_EMAIL":
        message = "Định dạng Email không đúng";
        break;
      case "ERROR_USER_NOT_FOUND":
        message = "Tài khoản không tồn tại";
        break;
      case "ERROR_WRONG_PASSWORD":
        message = "Sai mật khẩu, vui lòng nhập lại";
        break;
      case "ERROR_TOO_MANY_REQUESTS":
        message = "Quá giới hạn số lần đăng nhập, xin hãy thử lại sau vài phút";
        break;
      default:
        print(err);
        message = "Lỗi Đăng Nhập";
    }
    return message;
  }
}
