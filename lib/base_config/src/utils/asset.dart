class Assets {
  static const home_icon = 'assets/images/logo.png';
  static const google_icon = 'assets/images/google.png';
  static const bg_splash = 'assets/images/bg_splash.jpg';
  static const mcom = 'assets/images/mcom.png';
  static const bg_listbook = 'assets/images/bg_listbook.jpg';
  static const covid = 'assets/images/covid.jpg';
  static const bg_bookdetail = 'assets/images/bg-baby.png';
  static const medical2 = 'assets/images/medical_2.png';
  static const bg_doctor = 'assets/images/bg_doctor.png';
}
