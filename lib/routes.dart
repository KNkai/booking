import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:super_init_app/main-booking.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking-dashboard.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/page/booking_doctor_info_page.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/page/booking_doctor_list_page.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_info/bookingInfo.page.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_list/bookingList.page.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_schedule/bookingSchedule_page.dart';


import 'base_config/base_config.dart';
import 'src/modules/report/shake_detector.dart';

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

class RoutePaths {
  static const String splash = '/splash';
  static const String login = '/login';
  static const String home = '/home';
  static const String notification = '/notification';
  static const String notificationDetail = '/notificationDetail';

  static const String scanqr = '/scanqr';
  static const String historyOrder = '/historyOrder';
  static const String profile = '/profile';
  static const String store = '/store';
  static const String selectStore = '/selectStore';
  static const String storeDetail = '/storeDetail';
  static const String selectTopic = '/selectTopic';
  static const String post = '/post';
  static const String oneTopic = '/oneTopic';
  static const String cartHistory = '/cartHistory';
  static const String rootDetailLitter = '/rootDetail';
  static const String cart = '/cart';
  static const String pay = '/pay';

  static const String cartPhotoView = '/cartPhotoView';
  static const String cartPopupAgencies = '/cartPopupAgencies';

  static const String pointHistory = '/pointHistory';
  static const String listWheelOfFortune = '/listWheelOfFortune';
  static const String giftHistory = "/giftHistory";
  static const String changePassword = "/changePassword";
  static const String report = "/report";

  static const String bookingList = "/bookingList";
  static const String bookingInfo = "/bookingInfo";
  static const String bookingDoctorList = "/bookingDoctorList";
  static const String bookingDoctor = "/bookingDoctor";

  static const String bookingSchedule = "/bookingSchedule";
  static const String bookingDashboard = "bookingDashboard";
}

Route<dynamic> onGenerateRoute(RouteSettings settings) {
  switch (settings.name) {
    //for booking
    case RoutePaths.bookingList:
      return MaterialPageRoute(
        builder: (context) => BookingList(),
      );
    case RoutePaths.bookingInfo:
      return MaterialPageRoute(
        builder: (context) => BookingInfo(settings.arguments as String),
      );
    case RoutePaths.bookingDashboard:
      return MaterialPageRoute(
        builder: (context) => BookingDashboard(),
      );
    case RoutePaths.bookingDoctorList:
      return MaterialPageRoute(
        builder: (context) => BookingDoctorListPage(),
      );
    case RoutePaths.bookingDoctor:
      return MaterialPageRoute(
        builder: (context) =>
            BookingDoctorInfoPage(settings.arguments as String),
      );
    case RoutePaths.bookingSchedule:
      return MaterialPageRoute(
        builder: (context) => BookingSchedule(),
      );
    //for booking

    

    default:
      return MaterialPageRoute(
        builder: (context) => Scaffold(
          body: Center(
            child: Text('Đường dẫn đến ${settings.name} không tồn tại'),
          ),
        ),
      );
  }
}

pageRouteWithShakeDetector(Widget child) {
  return MaterialPageRoute(
    builder: (context) => ShakeDetectorWidget(
      child: child,
      onShake: () => ScreenShot.screenShot(context, appKey),
      navigatorKey: navigatorKey,
    ),
  );
}
