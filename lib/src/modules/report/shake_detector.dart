import 'package:flutter/material.dart';
import 'package:shake/shake.dart';

import 'bug_report.dart';


class ShakeDetectorWidget extends StatefulWidget {
  final Widget child;
  final Function onShake;
  final GlobalKey<NavigatorState> navigatorKey;  
  ShakeDetectorWidget({
    @required this.child,
    @required this.navigatorKey,
    this.onShake,
  });
  @override
  _ShakeDetectorWidgetState createState() => _ShakeDetectorWidgetState();
}

class _ShakeDetectorWidgetState extends State<ShakeDetectorWidget> {
  static ShakeDetector detector;

  @override
  void dispose() {
    detector.stopListening();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (detector!=null) return;
    detector = ShakeDetector.autoStart(onPhoneShake: () {
      detector.stopListening();
      if (widget.onShake != null) {
        widget.onShake();
      }
      showReportBottomSheet(context, widget.navigatorKey).then((_) {
        detector.startListening();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
