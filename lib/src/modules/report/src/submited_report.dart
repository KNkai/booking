
import 'package:super_init_app/base_config/base_config.dart';

import 'package:flutter/material.dart';

showSubmitReport(BuildContext context, GlobalKey<NavigatorState> navigatorKey) {
  showGeneralDialog(
      context: context,
      barrierColor: Colors.black12.withOpacity(0.6), // background color
      barrierDismissible:
          true, // should dialog be dismissed when tapped outside
      barrierLabel: "Dialog", // label for barrier
      transitionDuration: Duration(
          milliseconds:
              400), // how long it takes to popup dialog after button click
      pageBuilder: (_, __, ___) {
        return Center(
          child: Material(
            color: Colors.transparent,
            child: InkWell(
              onTap: () => navigatorKey.currentState.pop(),
              child: Container(
                height: SizeConfig.heightMultiplier * 50,
                width: SizeConfig.widthMultiplier * 85,
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(
                    Radius.circular(10.0),
                  ),
                  color: Colors.white,
                ),
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 25),
                child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Xin cảm ơn",
                        textAlign: TextAlign.center,
                        style: ptTitle(context).copyWith(color: Colors.black87),
                      ),
                      Expanded(child: Image.asset('assets/images/saved.jpg')),
                      Text(
                        "Báo cáo sự cố của bạn đã được ghi lại, chúng tôi sẽ sớm khắc phục",
                        textAlign: TextAlign.center,
                        style: 
                            ptSubhead(context).copyWith(color: Colors.black87),
                      ),
                    ]),
              ),
            ),
          ),
        );
      });
}
