import 'package:super_init_app/base_config/base_config.dart';

import 'package:flutter/material.dart';

Future showReportBottomSheet(BuildContext context, GlobalKey<NavigatorState> navigatorkey) {
  return showModalBottomSheet<void>(
      isScrollControlled: true,
      context: context,
      backgroundColor: Colors.transparent,
      builder: (BuildContext context) {
        return ReportBottomSheet(navigatorKey: navigatorkey);
      });
}

class ReportBottomSheet extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  ReportBottomSheet({@required this.navigatorKey});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Container(
        margin: EdgeInsets.only(top: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
          color: Colors.white,
        ),
        child: Column(
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20.0),
                    topRight: Radius.circular(20.0)),
                color: CustomColor.main,
              ),
              padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Center(
                    child: Text(
                      'Báo lỗi ứng dụng',
                      style: ptTitle(context).copyWith(color: Colors.white),
                    ),
                  )),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
              child: Column(
                children: <Widget>[
                  Text(
                    "Phản hồi của bạn giúp đội ngũ phát triển cải thiện phần mềm tốt hơn",
                    textAlign: TextAlign.center,
                    style: ptSubhead(context).copyWith(color: Colors.black87),
                  ),
                  SizedBox(
                      height: SizeConfig.heightMultiplier * 20,
                      child: Image.asset('assets/images/shake.gif')),
                  Align(
                    alignment: Alignment.center,
                    child: SizedBox(
                      width: SizeConfig.widthMultiplier * 90,
                      height: SizeConfig.heightMultiplier * 6.3,
                      child: RaisedButton(
                        elevation: 1,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(color: Colors.grey),
                        ),
                        onPressed: () {
                          //navigatorKey.currentState.pop();
                          navigatorKey.currentState.pushNamed('/report');
                        },
                        color: CustomColor.main,
                        textColor: Colors.white,
                        child: Text(
                          'Chuyển tới trang báo sự cố',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: SizeConfig.textMultiplier * 2.5,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: SizeConfig.heightMultiplier * 2,
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
