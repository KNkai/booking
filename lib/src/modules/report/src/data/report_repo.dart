
import 'package:flutter/widgets.dart';
import 'package:super_init_app/src/modules/report/src/data/report_srv.dart';
import 'package:super_init_app/src/modules/report/src/model/clientLog_model.dart';

class ReportRepo {
  ReportSrv _reportSrv;
  ReportRepo({@required ReportSrv reportSrv}) : _reportSrv = reportSrv;

  Future<void> createClientLog(ClientLogModel input) async {
    var data = 'data { note:"${input.note}", image:"${input.image}" }';
    await _reportSrv.mutate('createClientLog', data,
        fragment: 'image note');
  }
}

