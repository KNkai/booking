
import 'package:super_init_app/base_config/base_config.dart';

class ReportSrv extends BaseServices {
  ReportSrv() : super(module: 'ClientLog', fragment: ''' 
  _id: ID
  image: String
  note: String
  ip: String
  userAgent: String
  createdAt: DateTime
  updatedAt: DateTime
  ''');
}
