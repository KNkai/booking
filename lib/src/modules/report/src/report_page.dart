
import 'package:super_init_app/base_config/base_config.dart';


import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:super_init_app/base_widget/base_widget.dart';

import 'data/report_repo.dart';
import 'data/report_srv.dart';
import 'events/allImageRemoved_event.dart';
import 'events/deleteImage_event.dart';
import 'events/editImage_event.dart';
import 'events/sendReportFailed_event.dart';
import 'events/sendReportSuccess_event.dart';
import 'events/sendReport_event.dart';
import 'events/uploadImage_event.dart';
import 'report_bloc.dart';
import 'submited_report.dart';

class ReportPage extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  ReportPage({@required this.navigatorKey});
  @override
  Widget build(BuildContext context) {
    return BasePageContainer(
      title: 'Trang báo lỗi',
      di: [
        Provider.value(value: ReportSrv()),
        ProxyProvider<ReportSrv, ReportRepo>(
          update: (context, reportSrv, previous) =>
              ReportRepo(reportSrv: reportSrv),
        )
      ],
      bloc: [],
      child: ReportProvider(navigatorKey: navigatorKey,),
    );
  }
}

class ReportProvider extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  ReportProvider({@required this.navigatorKey});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => ReportBloc(reportRepo: Provider.of(context)),
        child: ReportBody(navigatorKey: navigatorKey,));
  }
}

class ReportBody extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  ReportBody({@required this.navigatorKey});
  @override
  _ReportBodyState createState() => _ReportBodyState();
}

class _ReportBodyState extends State<ReportBody> with BlocCreator {
  ReportBloc reportBloc;
  TextEditingController _textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (reportBloc == null) {
      reportBloc = Provider.of<ReportBloc>(context);
    }
  }

  handleEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case UploadImageEvent:
        UploadImageEvent e = event as UploadImageEvent;
        final snackBar = SnackBar(
          duration: const Duration(milliseconds: 5000),
          content: Text("Ảnh đang được gửi đi, vui lòng chờ trong giây lát"),
          backgroundColor: CustomColor.main,
        );
        Scaffold.of(context).showSnackBar(snackBar);
        return;
      case SendReportFailedEvent:
        SendReportFailedEvent e = event as SendReportFailedEvent;
        final snackBar = SnackBar(
          duration: const Duration(milliseconds: 1500),
          content: Text(e.err),
          backgroundColor: Colors.redAccent,
        );
        Scaffold.of(context).showSnackBar(snackBar);
        return;
      case SendReportSuccessEvent:
        SendReportSuccessEvent e = event as SendReportSuccessEvent;
        widget.navigatorKey.currentState.pop();
        widget.navigatorKey.currentState.pop();
        showSubmitReport(context, widget.navigatorKey);
        return;
      case AllImageRemovedEvent:
        AllImageRemovedEvent e = event as AllImageRemovedEvent;
        widget.navigatorKey.currentState.pop();
        widget.navigatorKey.currentState.pop();
        return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return LoadingTask(
      bloc: reportBloc,
      child: BlocListener<ReportBloc>(
        listener: handleEvent,
        child: Scaffold(
          appBar: AppBarCustomNew(
            title: 'Báo cáo lỗi',
          ),
          body: Container(
            padding: EdgeInsets.only(top: 15, left: 15, right: 15),
            child: Column(
              children: <Widget>[
                TextField(
                    controller: _textEditingController,
                    decoration: InputDecoration(
                        hintText: 'Vui lòng mô tả lỗi mà bạn nhận được')),
                SizedBox(
                  height: SizeConfig.heightMultiplier * 5,
                ),
                Container(
                  width: SizeConfig.widthMultiplier * 100,
                  height: SizeConfig.heightMultiplier * 30,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: ScreenShot.listUint8List.length,
                    itemBuilder: (context, index) {
                      return Stack(
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(12),
                            child: Image.memory(
                              ScreenShot.listUint8List[index],
                              fit: BoxFit.contain,
                            ),
                          ),
                          Positioned(
                            left: SizeConfig.widthMultiplier * 6,
                            top: SizeConfig.heightMultiplier * 5,
                            child: InkWell(
                              onTap: () {
                                reportBloc.add(EditImageEvent(index, context));
                              },
                              child: Container(
                                height: SizeConfig.heightMultiplier * 20,
                                width: SizeConfig.widthMultiplier * 20,
                                color: Colors.transparent,
                              ),
                            ),
                          ),
                          Positioned(
                            right: 5,
                            child: InkWell(
                              onTap: () =>
                                  reportBloc.add(DeleteImageEvent(index)),
                              child: Container(
                                height: 25,
                                width: 25,
                                child: CircleAvatar(
                                  backgroundColor: Colors.black54,
                                  child: Icon(
                                    Icons.close,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      );
                    },
                  ),
                ),
                SizedBox(
                  height: SizeConfig.heightMultiplier * 4,
                ),
                Text(
                  "* Quay lại và lắc nhiều lần để gửi nhiều hình *",
                  textAlign: TextAlign.center,
                  style: ptBody1(context).copyWith(color: Colors.black87),
                ),
                SizedBox(
                  height: SizeConfig.heightMultiplier * 4,
                ),
                Align(
                  alignment: Alignment.center,
                  child: SizedBox(
                    width: SizeConfig.widthMultiplier * 30,
                    height: SizeConfig.heightMultiplier * 6,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                        side: BorderSide(color: Colors.orange),
                      ),
                      onPressed: () {
                        reportBloc.add(SendReportEvent(_textEditingController.text));
                      },
                      color: Colors.orange,
                      textColor: Colors.white,
                      child: Text(
                        'Báo lỗi',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: SizeConfig.textMultiplier * 2.5,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
