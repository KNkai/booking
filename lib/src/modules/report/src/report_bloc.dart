import 'dart:io';

import 'package:super_init_app/base_config/base_config.dart';

import 'package:flutter/cupertino.dart';

import 'data/report_repo.dart';
import 'events/allImageRemoved_event.dart';
import 'events/deleteImage_event.dart';
import 'events/editImage_event.dart';
import 'events/sendReportFailed_event.dart';
import 'events/sendReportSuccess_event.dart';
import 'events/sendReport_event.dart';
import 'events/uploadImage_event.dart';
import 'model/clientLog_model.dart';


class ReportBloc extends BaseBloc with ChangeNotifier {
  ReportBloc({@required ReportRepo reportRepo}) {
    _reportRepo = reportRepo;
  }

  ReportRepo _reportRepo;

  @override
  void dispatchEvent(BaseEvent event) {
    if (event is DeleteImageEvent) {
      handleDeleteImageEvent(event);
    }
    if (event is SendReportEvent) {
      handleSendReportEvent(event);
    }
    if (event is EditImageEvent) {
      handleEditImageEvent(event);
    }
  }

  handleSendReportEvent(SendReportEvent event) async {
    try {
      loadingSink.add(true);
      processEventSink.add(UploadImageEvent());
      for (int i = 0; i < ScreenShot.listUint8List.length; i++) {
        File file = await ImageUtil.createImageFileFromUint8List(
            ScreenShot.listUint8List[i]);
        String url = await ImageUtil.upload(file);
        _reportRepo.createClientLog(
            ClientLogModel(image: url, note: event.note));
      }

      ScreenShot.clearAllImage();
      processEventSink.add(SendReportSuccessEvent());
    } catch (e) {
      print(e);
      processEventSink.add(SendReportFailedEvent(
          "Gửi hình ảnh thất bại, xin hãy kiểm tra kết nối mạng hoặc thử lại sau"));
    } finally {
      loadingSink.add(false);
    }
  }

  handleDeleteImageEvent(DeleteImageEvent event) async {
    ScreenShot.removeScreenShot(event.pos);
    if (ScreenShot.listUint8List.length == 0) {
      processEventSink.add(AllImageRemovedEvent());
    }
    notifyListeners();
  }

  handleEditImageEvent(EditImageEvent event) async {
    loadingSink.add(true);
    await ScreenShot.drawOnImage(event.context, event.pos);
    Future.delayed(Duration(milliseconds: 500), () {
      notifyListeners();
      loadingSink.add(false);
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}
