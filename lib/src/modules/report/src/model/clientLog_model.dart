class ClientLogModel {
  String note;
  String image;
  ClientLogModel({this.note, this.image});

  factory ClientLogModel.fromMap(Map<String, dynamic> map) {
    return ClientLogModel(
        note: map["note"].toString(), image: map["image"].toString());
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      "note": note,
      "image": image,
    };
  }
}
