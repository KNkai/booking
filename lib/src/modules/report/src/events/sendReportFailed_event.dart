import 'package:super_init_app/base_config/base_config.dart';


class SendReportFailedEvent extends BaseEvent {
  String err;
  SendReportFailedEvent(this.err);
}
