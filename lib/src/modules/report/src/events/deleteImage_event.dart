
import 'package:super_init_app/base_config/base_config.dart';


class DeleteImageEvent extends BaseEvent {
  int pos;
  DeleteImageEvent(this.pos);
}
