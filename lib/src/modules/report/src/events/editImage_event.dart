import 'package:super_init_app/base_config/base_config.dart';

import 'package:flutter/material.dart';

class EditImageEvent extends BaseEvent {
  int pos;
  BuildContext context;
  EditImageEvent(this.pos, this.context);
}
