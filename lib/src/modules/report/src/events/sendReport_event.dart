import 'package:super_init_app/base_config/base_config.dart';


class SendReportEvent extends BaseEvent {
  String note;
  SendReportEvent(this.note);
}
