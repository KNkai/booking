import 'dart:ui';

import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/base_config/src/utils/asset.dart';
import 'package:super_init_app/routes.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_instance.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(Assets.bg_listbook),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 40),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Chào mừng bạn đến với",
                        style: ptTitle(context).copyWith(
                            fontWeight: FontWeight.w900,
                            color: HexColor("#152066")),
                      ),
                      Text(
                        "Datlich.mcom.app",
                        style: ptTitle(context).copyWith(
                            fontWeight: FontWeight.w600,
                            color: HexColor("#152066")),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Container(
                  child: Column(
                    children: <Widget>[
                      ClipRRect(
                          borderRadius:
                              BorderRadius.vertical(top: Radius.circular(10.0)),
                          child: Image.asset(Assets.covid)),
                      Container(
                        padding: EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.vertical(
                                bottom: Radius.circular(10.0))),
                        child: Text(
                          "Miễn phí tham vấn sức khỏe trực tuyến với bác sĩ chuyên khoa 24/7",
                          style: ptSubtitle(context).copyWith(
                              fontWeight: FontWeight.w900,
                              color: Colors.indigo[700]),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.lightBlue[100],
                onTap: () {
                  BookingInstance.instance.clearData();
                  navigatorKey.currentState.pushNamed(RoutePaths.bookingList);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Container(
                            width: 100,
                            height: 100,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(50),
                                ),
                                gradient: LinearGradient(colors: [
                                  Colors.indigo[200],
                                  Colors.indigo[100]
                                ])),
                            child: Center(
                                child: SizedBox(
                              height: 120,
                              width: 120,
                              child: Image.asset('assets/images/bac-si.png'),
                            )),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            "Đặt lịch khám bệnh",
                            maxLines: 2,
                            overflow: TextOverflow.clip,
                            style: TextStyle(
                                color: Colors.indigo[500],
                                fontWeight: FontWeight.w700,
                                fontSize: 20),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
              InkWell(
                splashColor: Colors.lightBlue[100],
                onTap: () {
                  navigatorKey.currentState.pushNamed(RoutePaths.bookingList);
                },
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10.0))),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Container(
                            child: Center(
                                child: SizedBox(
                              child: Image.asset(
                                'assets/images/calendar.png',
                                height: 100,
                                width: 100,
                              ),
                            )),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            "Lịch sử đặt lịch",
                            maxLines: 2,
                            overflow: TextOverflow.clip,
                            style: TextStyle(
                                color: Colors.indigo[500],
                                fontWeight: FontWeight.w700,
                                fontSize: 20),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
