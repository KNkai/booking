import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/base_widget/base_widget.dart';
import 'package:super_init_app/routes.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/bloc/list_doctor_bloc.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/data/doctor_repo.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/data/doctor_srv.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/event/list_doctor_event.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/state/list_doctor_state.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_instance.dart';

class BookingDoctorListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasePageContainer(
      di: [
        Provider.value(value: DoctorSrv()),
        ProxyProvider<DoctorSrv, DoctorRepo>(
          update: (context, srv, previous) => DoctorRepo(srv: srv),
        )
      ],
      bloc: [],
      child: DoctorProvider(),
    );
  }
}

class DoctorProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => ListDoctorBloc(repo: Provider.of(context)),
        child: ListDoctorBody());
  }
}

class ListDoctorBody extends StatefulWidget {
  @override
  _ListDoctorBodyState createState() => _ListDoctorBodyState();
}

class _ListDoctorBodyState extends State<ListDoctorBody> with BlocCreator {
  ListDoctorBloc _bloc;

  @override
  void initState() {
    if (_bloc == null) {
      _bloc = Provider.of(context, listen: false);
      _bloc.add(LoadListDoctorEvent());
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.maybePop(context);
            },
            child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Icon(
                  Icons.navigate_before,
                  color: Colors.black.withOpacity(0.7),
                  size: 30,
                )),
          ),
          actions: <Widget>[
            Center(
              child: IconButton(
                icon: Icon(EvaIcons.home, color: Colors.black.withOpacity(0.7),),
                onPressed: () {
                  navigatorKey.currentState.popUntil((route) => route.isFirst);
                },
              ),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 0),
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "Gặp các bác sĩ của chúng tôi",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: SizeConfig.textMultiplier * 3.5,
                          color: CustomColor.colorTextDeepBlue,
                        ),
                      ),
                      SpacingBox(
                        height: 1.5,
                      ),
                      Text(
                        "Ngày giờ hiển thị theo giờ Việt Nam",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: SizeConfig.textMultiplier * 2.2,
                          color: CustomColor.colorTextDeepBlue,
                        ),
                      ),
                      SpacingBox(
                        height: 0.2,
                      ),
                      Text(
                        "(GMT+7:00)",
                        style: TextStyle(
                          fontWeight: FontWeight.w800,
                          fontSize: SizeConfig.textMultiplier * 2.2,
                          color: CustomColor.colorTextDeepBlue,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: StreamBuilder<ListDoctorState>(
                    stream: _bloc.stateListDoctor.stream,
                    builder: (context, snapshot) {
                      if (snapshot.data is LoadingListDoctorState)
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      else if (snapshot.data is LoadListDoctorFailState) {
                        return ErrorMessage(state: snapshot.data);
                      } else if (snapshot.data is LoadListDoctorSuccessState)
                        return ListDoctorWidget(
                          state: snapshot.data,
                        );
                    }),
              ),
            ],
          ),
        ));
  }
}

class ErrorMessage extends StatelessWidget {
  final LoadListDoctorFailState state;

  const ErrorMessage({
    Key key,
    this.state,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(state.errMessage),
    );
  }
}

class ListDoctorWidget extends StatelessWidget {
  final LoadListDoctorSuccessState state;
  const ListDoctorWidget({
    Key key,
    this.state,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(state.data);
    return ListView.builder(
        itemCount: state.data['data'].length,
        shrinkWrap: true,
        physics: ScrollPhysics(),
        itemBuilder: (BuildContext context, int index) {
          dynamic item = state.data['data'][index];
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 5),
            child: Card(
              child: ListTile(
                onTap: () {
                  navigatorKey.currentState.pushNamed(RoutePaths.bookingDoctor,
                      arguments: item["_id"]);
                },
                isThreeLine: true,
                leading: Container(
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(50),
                        ),
                        gradient: LinearGradient(
                            colors: [Colors.indigo[200], Colors.indigo[100]])),
                    child: Image.asset('assets/images/bac-si.png')),
                title: Text(item['name']),
                subtitle: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      item['branch']['name'],
                      style: ptCaption(context)
                          .copyWith(fontStyle: FontStyle.italic),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(50),
                          ),
                          gradient: LinearGradient(colors: [
                            Colors.indigo[200],
                            Colors.indigo[100]
                          ])),
                      child: Padding(
                        padding: const EdgeInsets.all(5.0),
                        child: Text(
                          "Giờ đặt tiếp theo 15/06/2020 14:00",
                          style: ptCaption(context),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }
}
