import 'dart:ui';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';

import 'package:provider/provider.dart';
import 'package:super_init_app/routes.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/bloc/doctor_info_bloc.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/data/doctor_repo.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/data/doctor_srv.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/event/doctor_info_event.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/state/doctor_info_state.dart';

import '../../booking_instance.dart';

class BookingDoctorInfoPage extends StatelessWidget {
  final String id;
  BookingDoctorInfoPage(this.id);
  @override
  Widget build(BuildContext context) {
    return BasePageContainer(
      title: 'Trang danh sach Doctor',
      di: [
        Provider.value(value: DoctorSrv()),
        ProxyProvider<DoctorSrv, DoctorRepo>(
          update: (context, srv, previous) => DoctorRepo(srv: srv),
        )
      ],
      bloc: [],
      child: DoctorInfoProvider(id),
    );
  }
}

class DoctorInfoProvider extends StatelessWidget {
  final String id;
  DoctorInfoProvider(this.id);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => DoctorInfoBloc(repo: Provider.of(context)),
        child: DoctorInfoBody(id));
  }
}

class DoctorInfoBody extends StatefulWidget {
  final String id;
  DoctorInfoBody(this.id);

  @override
  _DoctorInfoBodyState createState() => _DoctorInfoBodyState();
}

class _DoctorInfoBodyState extends State<DoctorInfoBody> with BlocCreator {
  DoctorInfoBloc _bloc;

  @override
  void initState() {
    if (_bloc == null) {
      _bloc = Provider.of(context, listen: false);
      _bloc.add(LoadDoctorInfoEvent(id: widget.id));
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        BookingInstance.instance.selectedDoctor = null;
        return Future.value(true);
      },
      child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            leading: GestureDetector(
              onTap: () {
                Navigator.maybePop(context);
              },
              child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Icon(
                    Icons.navigate_before,
                    color: Colors.black.withOpacity(0.7),
                    size: 30,
                  )),
            ),
            actions: <Widget>[
              Center(
                child: IconButton(
                  icon: Icon(EvaIcons.home),
                  color: Colors.black54,
                  onPressed: () {
                    navigatorKey.currentState
                        .popUntil((route) => route.isFirst);
                  },
                ),
              ),
            ],
          ),
          bottomNavigationBar: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Material(
                    elevation: 1,
                    borderRadius: BorderRadius.all(
                      Radius.circular(SizeConfig.widthMultiplier * 7),
                    ),
                    color: CustomColor.lightGreen,
                    child: InkWell(
                      splashColor: Colors.blue,
                      borderRadius: BorderRadius.all(
                        Radius.circular(SizeConfig.widthMultiplier * 7),
                      ),
                      onTap: () {
                        BookingInstance.instance.selectedDoctor = _bloc.doctor;
                        if (BookingInstance.instance.selectedTime == null) {
                          navigatorKey.currentState
                              .pushNamed(RoutePaths.bookingSchedule);
                        }
                      },
                      child: Container(
                        height: SizeConfig.heightMultiplier * 6,
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                        child: Center(
                          child: Text(
            
                            "Tiếp tục",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: SizeConfig.textMultiplier * 2.3,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          body: StreamBuilder<DoctorInfoState>(
              stream: _bloc.stateDoctorInfo.stream,
              builder: (context, snapshot) {
                if (snapshot.data is LoadingDoctorInfoState)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                else if (snapshot.data is LoadDoctorInfoFailState) {
                  return ErrorMessage(state: snapshot.data);
                } else if (snapshot.data is LoadDoctorInfoSuccessState)
                  return DoctorInfoWidget(
                    state: snapshot.data,
                  );
              })),
    );
  }
}

class ErrorMessage extends StatelessWidget {
  final LoadDoctorInfoFailState state;

  const ErrorMessage({
    Key key,
    this.state,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(state.errMessage),
    );
  }
}

class DoctorInfoWidget extends StatelessWidget {
  final LoadDoctorInfoSuccessState state;

  const DoctorInfoWidget({Key key, this.state}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    dynamic data = state.data;
    return ListView(
      physics: ScrollPhysics(),
      children: <Widget>[
        Container(
          height: deviceHeight(context) * 0.4,
          child: Stack(
            children: <Widget>[
              Container(
                height: deviceHeight(context) * 0.4,
                child: Image.asset(
                  'assets/images/bg_doctor_info.png',
                  fit: BoxFit.fitHeight,
                ),
              ),
              Center(
                child: Image.asset(
                  'assets/images/bac-si.png',
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
          child: Center(
            child: Column(
              children: <Widget>[
                Text(
                  data['name'],
                  style: ptTitle(context).copyWith(
                      fontWeight: FontWeight.w900, color: HexColor("#0B5AA9")),
                ),
                Text(data['intro'] ?? 'Chưa cập nhật',
                    style: ptCaption(context)),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        color: Colors.indigo[50]),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          EvaIcons.thermometerPlusOutline,
                          color: CustomColor.main,
                          size: 50,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Chuyên ngành",
                              style: ptSubtitle(context).copyWith(
                                  fontWeight: FontWeight.w700,
                                  color: HexColor("#0B5AA9")),
                            ),
                            Text(data['branch']['name'] ?? 'Chưa cập nhật',
                                style: ptCaption(context)),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        color: Colors.indigo[50]),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          EvaIcons.thermometerPlusOutline,
                          color: CustomColor.main,
                          size: 50,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Chuyên ngành",
                              style: ptSubtitle(context).copyWith(
                                  fontWeight: FontWeight.w700,
                                  color: HexColor("#0B5AA9")),
                            ),
                            Text(data['branch']['name'] ?? 'Chưa cập nhật',
                                style: ptCaption(context)),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        color: Colors.indigo[50]),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          EvaIcons.thermometerPlusOutline,
                          color: CustomColor.main,
                          size: 50,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Chuyên ngành",
                              style: ptSubtitle(context).copyWith(
                                  fontWeight: FontWeight.w700,
                                  color: HexColor("#0B5AA9")),
                            ),
                            Text(data['branch']['name'] ?? 'Chưa cập nhật',
                                style: ptCaption(context)),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(
                          Radius.circular(5.0),
                        ),
                        color: Colors.indigo[50]),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          EvaIcons.thermometerPlusOutline,
                          color: CustomColor.main,
                          size: 50,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "Chuyên ngành",
                              style: ptSubtitle(context).copyWith(
                                  fontWeight: FontWeight.w700,
                                  color: HexColor("#0B5AA9")),
                            ),
                            Text(data['branch']['name'] ?? 'Chưa cập nhật',
                                style: ptCaption(context)),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
