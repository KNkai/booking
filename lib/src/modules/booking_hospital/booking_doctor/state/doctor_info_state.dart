import 'package:super_init_app/base_config/base_config.dart';

class DoctorInfoState extends BaseEvent {}

class LoadingDoctorInfoState extends DoctorInfoState {}

class LoadDoctorInfoSuccessState extends DoctorInfoState {
  final dynamic data;
  LoadDoctorInfoSuccessState({this.data});
}

class LoadDoctorInfoFailState extends DoctorInfoState {
  final String errMessage;
  LoadDoctorInfoFailState({this.errMessage});
}
