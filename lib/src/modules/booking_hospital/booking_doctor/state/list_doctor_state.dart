import 'package:super_init_app/base_config/base_config.dart';

class ListDoctorState extends BaseEvent {}

class LoadingListDoctorState extends ListDoctorState {}

class LoadListDoctorSuccessState extends ListDoctorState {
  final dynamic data;
  LoadListDoctorSuccessState({this.data});
}

class LoadListDoctorFailState extends ListDoctorState {
  final String errMessage;
  LoadListDoctorFailState({this.errMessage});
}
