import 'dart:async';

import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/data/doctor_repo.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/event/list_doctor_event.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/state/list_doctor_state.dart';

class ListDoctorBloc extends BaseBloc with ChangeNotifier {
  ListDoctorBloc({@required DoctorRepo repo}) {
    _repo = repo;
  }

  DoctorRepo _repo;
  final StreamController<ListDoctorState> stateListDoctor = StreamController();

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case LoadListDoctorEvent:
        handleLoadListDoctor();
    }
  }

  @override
  void dispose() {
    stateListDoctor.close();
    super.dispose();
  }

  handleLoadListDoctor() {
    stateListDoctor.sink.add(LoadingListDoctorState());
    try {
      _repo.getListDoctor().then((res) {
        stateListDoctor.sink.add(LoadListDoctorSuccessState(data: res));
      });
    } catch (e) {
      stateListDoctor.sink.add(LoadListDoctorFailState(errMessage: e.toString()));
    }
  }
}
