import 'dart:async';

import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/data/doctor_repo.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/event/doctor_info_event.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/state/doctor_info_state.dart';

class DoctorInfoBloc extends BaseBloc with ChangeNotifier {
  DoctorInfoBloc({@required DoctorRepo repo}) {
    _repo = repo;
  }

  DoctorRepo _repo;
  dynamic doctor;
  final StreamController<DoctorInfoState> stateDoctorInfo = StreamController();

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case LoadDoctorInfoEvent:
        handleLoadDoctorInfo(event);
    }
  }

  @override
  void dispose() {
    stateDoctorInfo.close();
    super.dispose();
  }

  handleLoadDoctorInfo(LoadDoctorInfoEvent event) {
    stateDoctorInfo.sink.add(LoadingDoctorInfoState());
    try {
      _repo.getItem(event.id).then((res) {
        stateDoctorInfo.sink.add(LoadDoctorInfoSuccessState(data: res));
        doctor = res;
      });
    } catch (e) {
      stateDoctorInfo.sink.add(LoadDoctorInfoFailState(errMessage: e.toString()));
    }
  }
}
