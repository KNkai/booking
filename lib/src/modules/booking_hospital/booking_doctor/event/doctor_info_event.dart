import 'package:super_init_app/base_config/base_config.dart';

class DoctorInfoEvent extends BaseEvent{}

class LoadDoctorInfoEvent extends DoctorInfoEvent{
  final String id;

  LoadDoctorInfoEvent({this.id});
}