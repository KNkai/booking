import 'package:super_init_app/base_config/src/base/base_service.dart';

class DoctorSrv extends BaseService {
  DoctorSrv() : super(module: "Doctor", fragment: ''' 
_id: ID
createdAt: DateTime
updatedAt: DateTime
branchId: String
code: String
name: String
photo: String
specialize: String
degree: [String]
experience: [String]
intro: String
requireSelect: Boolean
branch{_id,name}: Branch
  ''');
}
