import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_doctor/data/doctor_srv.dart';

class DoctorRepo {
  DoctorSrv _srv;
  DoctorRepo({@required DoctorSrv srv}) : _srv = srv;

  Future<dynamic> getListDoctor() async {
    var res = await _srv.getList();
    return res;
  }
    Future<dynamic> getItem(String id) async {
    var res = await _srv.getItem(id);
    return res;
  }
}
