import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';

import 'bookingSchedule_repo.dart';
import 'event/getbookingSchedule_event.dart';

class BookingScheduleBloc extends BaseBloc with ChangeNotifier {
  BookingScheduleBloc({@required BookingScheduleRepo bookingScheduleRepo}) {
    _bookingScheduleRepo = bookingScheduleRepo;
  }

  BookingScheduleRepo _bookingScheduleRepo;

  final _package$ = BehaviorSubject<dynamic>();
  Stream<dynamic> get packageStream => _package$.stream;
  Sink<dynamic> get packageSink => _package$.sink;
  dynamic get packageValue => _package$.value;

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case GetBookingScheduleEvent:
        handleGetBookingScheduleEvent(event);
        return;
      default:
    }
  }

  handleGetBookingScheduleEvent(GetBookingScheduleEvent event) async {
    try {
      loadingSink.add(true);
      final package =
          await _bookingScheduleRepo.getTimeFrameByPackageId(event.id);
      packageSink.add(package);
      loadingSink.add(false);
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }

  @override
  void dispose() {
    _package$.close();
    super.dispose();
  }
}
