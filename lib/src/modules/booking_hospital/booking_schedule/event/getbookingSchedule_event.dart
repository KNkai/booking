
import 'package:super_init_app/base_config/base_config.dart';

class GetBookingScheduleEvent extends BaseEvent {
  String id;
  GetBookingScheduleEvent(this.id);
}
