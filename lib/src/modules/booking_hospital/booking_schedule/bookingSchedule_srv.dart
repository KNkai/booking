




import 'package:super_init_app/base_config/src/base/base_service.dart';

class BookingScheduleSrv extends BaseService {
  BookingScheduleSrv() : super(module: 'TimeFrame', fragment: ''' 
_id
createdAt
updatedAt
packageId
branchId
startTime
endTime
name
  ''');
}