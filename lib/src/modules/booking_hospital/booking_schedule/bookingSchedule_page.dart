import 'dart:ui';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/base_widget/base_widget.dart';
import 'package:provider/provider.dart';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:super_init_app/routes.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_instance.dart';
import 'bookingSchedule_bloc.dart';
import 'bookingSchedule_repo.dart';
import 'bookingSchedule_srv.dart';
import 'event/getbookingSchedule_event.dart';

class BookingSchedule extends StatelessWidget {
  BookingSchedule();
  @override
  Widget build(BuildContext context) {
    return BasePageContainer(
      title: 'Trang danh sach booking',
      di: [
        Provider.value(value: BookingScheduleSrv()),
        ProxyProvider<BookingScheduleSrv, BookingScheduleRepo>(
          update: (context, bookingScheduleSrv, previous) =>
              BookingScheduleRepo(bookingScheduleSrv: bookingScheduleSrv),
        )
      ],
      bloc: [],
      child: BookingScheduleProvider(),
    );
  }
}

class BookingScheduleProvider extends StatelessWidget {
  BookingScheduleProvider();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) =>
            BookingScheduleBloc(bookingScheduleRepo: Provider.of(context)),
        child: BookingScheduleBody());
  }
}

class BookingScheduleBody extends StatefulWidget {
  BookingScheduleBody();

  @override
  _BookingScheduleBodyState createState() => _BookingScheduleBodyState();
}

class _BookingScheduleBodyState extends State<BookingScheduleBody>
    with BlocCreator {
  BookingScheduleBloc bookingScheduleBloc;
  ScrollController _scrollController = ScrollController();
  bool isShowBottomCard = true;
  dynamic itemSelect;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (bookingScheduleBloc == null) {
      bookingScheduleBloc = createBloc<BookingScheduleBloc>();
      bookingScheduleBloc.add(GetBookingScheduleEvent(
          BookingInstance.instance.selectedPackage["_id"]));
    }
  }

  handleEvent(BaseEvent event) {
    if (event is LoadingFailEvent) {
      return;
    }
    if (event is LoadingSuccessEvent) {
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        BookingInstance.instance.selectedTime = null;
        return Future.value(true);
      },
          child: LoadingTask(
        bloc: bookingScheduleBloc,
        child: BlocListener<BookingScheduleBloc>(
          listener: handleEvent,
          child: Scaffold(
            extendBodyBehindAppBar: true,
            appBar: AppBar(
              elevation: 0,
              leading: GestureDetector(
                onTap: () {
                  navigatorKey.currentState.maybePop();
                },
                child: Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Icon(
                      Icons.navigate_before,
                      color: Colors.black.withOpacity(0.7),
                      size: 30,
                    )),
              ),
              actions: <Widget>[
                Center(
                  child: IconButton(
                    icon:
                        Icon(EvaIcons.home, color: Colors.black.withOpacity(0.7)),
                    onPressed: () {
                      navigatorKey.currentState
                          .popUntil((route) => route.isFirst);
                    },
                  ),
                ),
              ],
            ),
            body: Stack(
              fit: StackFit.expand,
              children: <Widget>[
                // Container(
                //   decoration: BoxDecoration(
                //     image: DecorationImage(
                //       image: AssetImage(Assets.bg_doctor),
                //       fit: BoxFit.contain,
                //     ),
                //   ),
                // ),
                Positioned(
                  top: MediaQuery.of(context).padding.top,
                  child: ProgressLinear(1 / 8),
                ),
                bookingScheduleBloc.packageValue != null
                    ? Padding(
                        padding: EdgeInsets.only(top: 80),
                        child: RefreshIndicator(
                          onRefresh: () {
                            bookingScheduleBloc.event.add(GetBookingScheduleEvent(
                                BookingInstance.instance.selectedPackage["_id"]));
                            return Future.delayed(Duration(seconds: 1));
                          },
                          child: SingleChildScrollView(
                            controller: _scrollController,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.widthMultiplier * 5),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "Chọn ngày khám",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: SizeConfig.textMultiplier * 3.5,
                                        color: CustomColor.colorTextDeepBlue,
                                      ),
                                    ),
                                    SpacingBox(
                                      height: 1.5,
                                    ),
                                    Text(
                                      "Vui lòng chọn thời gian phù hợp với buổi khám",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: SizeConfig.textMultiplier * 2,
                                        color: Colors.black87,
                                      ),
                                    ),
                                    SpacingBox(
                                      height: 0.5,
                                    ),
                                    Text(
                                      "Ngày giờ hiển thị theo giờ Việt Nam",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: SizeConfig.textMultiplier * 2.2,
                                        color: CustomColor.colorTextDeepBlue,
                                      ),
                                    ),
                                    SpacingBox(
                                      height: 0.2,
                                    ),
                                    Text(
                                      "(GMT+7:00)",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize: SizeConfig.textMultiplier * 2.2,
                                        color: CustomColor.colorTextDeepBlue,
                                      ),
                                    ),
                                    SpacingBox(height: 1.5),
                                    CardOption(
                                        bookingScheduleBloc.packageValue["data"],
                                        DateTime.now(),
                                        itemSelect,
                                        _handleChangeValue),
                                    CardOption(
                                        bookingScheduleBloc.packageValue["data"],
                                        DateTime.now().add(Duration(days: 1)),
                                        itemSelect,
                                        _handleChangeValue),
                                    CardOption(
                                        bookingScheduleBloc.packageValue["data"],
                                        DateTime.now().add(Duration(days: 2)),
                                        itemSelect,
                                        _handleChangeValue),
                                    CardOption(
                                        bookingScheduleBloc.packageValue["data"],
                                        DateTime.now().add(Duration(days: 3)),
                                        itemSelect,
                                        _handleChangeValue),
                                    SpacingBox(height: 12),
                                  ]),
                            ),
                          ),
                        ),
                      )
                    : SizedBox.shrink(),
                isShowBottomCard
                    ? Positioned(
                        bottom: 0,
                        child: Material(
                          elevation: 3,
                          child: Container(
                            color: Colors.white,
                            padding: EdgeInsets.all(15),
                            width: MediaQuery.of(context).size.width,
                            child: itemSelect != null
                                ? CardAction(true, itemSelect)
                                : CardAction(false, itemSelect),
                          ),
                        ))
                    : SizedBox.shrink(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _handleChangeValue(val) {
    setState(() {
      itemSelect = val;
    });
  }

  _handleSubmit() {}
}

class CardOption extends StatelessWidget {
  final DateTime date;
  final dynamic item;
  final dynamic groupvalue;
  final Function handleChangeValue;
  CardOption(this.item, this.date, this.groupvalue, this.handleChangeValue);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: SizeConfig.heightMultiplier * 2),
      decoration: BoxDecoration(
        color: HexColor("F5F8FD"),
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(SizeConfig.widthMultiplier * 7),
          topLeft: Radius.circular(SizeConfig.widthMultiplier * 7),
          bottomLeft: Radius.circular(SizeConfig.widthMultiplier * 3),
          bottomRight: Radius.circular(SizeConfig.widthMultiplier * 3),
        ),
      ),
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        padding: EdgeInsets.only(top: 0),
        shrinkWrap: true,
        itemCount: item.length + 1,
        itemBuilder: (context, index) {
          if (index == 0) return Timeheader(date);
          return Column(
            children: <Widget>[
              ListTile(
                trailing: Radio(
                    value: item[index - 1]["_id"] + '/' + date.day.toString(),
                    onChanged: handleChangeValue,
                    groupValue: groupvalue),
                title: Text(
                    "${item[index - 1]["startTime"]} - ${item[index - 1]["endTime"]}"),
              ),
              index != item.length
                  ? Divider(
                      endIndent: 15,
                      indent: 15,
                    )
                  : SizedBox.shrink(),
            ],
          );
        },
      ),
    );
  }
}

class Timeheader extends StatelessWidget {
  final DateTime date;
  Timeheader(this.date);
  @override
  Widget build(BuildContext context) {
    return Material(
      borderRadius: BorderRadius.all(
        Radius.circular(SizeConfig.widthMultiplier * 7),
      ),
      elevation: 2,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(
            Radius.circular(SizeConfig.widthMultiplier * 7),
          ),
        ),
        height: SizeConfig.heightMultiplier * 6,
        padding: EdgeInsets.symmetric(horizontal: 15, vertical: 3),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "${date.day} tháng ${date.month}",
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: SizeConfig.textMultiplier * 2.3,
                color: CustomColor.colorTextDeepBlue),
          ),
        ),
      ),
    );
  }
}

class CardAction extends StatelessWidget {
  final dynamic selectedValue;
  final bool enable;
  CardAction(this.enable, this.selectedValue);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Material(
            elevation: 1,
            borderRadius: BorderRadius.all(
              Radius.circular(SizeConfig.widthMultiplier * 7),
            ),
            color: enable ? CustomColor.main : Colors.grey[400],
            child: InkWell(
              splashColor: enable ? CustomColor.main : Colors.grey[200],
              borderRadius: BorderRadius.all(
                Radius.circular(SizeConfig.widthMultiplier * 7),
              ),
              onTap: () {
                if (enable) {
                  BookingInstance.instance.selectedTime = selectedValue;
                  if (BookingInstance.instance.selectedDoctor == null) {
                    navigatorKey.currentState
                        .pushNamed(RoutePaths.bookingDoctorList);
                  }
                }
              },
              child: Container(
                height: SizeConfig.heightMultiplier * 6,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                child: Center(
                  child: Text(
                    "Tiếp tục",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: SizeConfig.textMultiplier * 2.3,
                        color: Colors.white),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class CardContent extends StatelessWidget {
  final String content;
  final List<dynamic> list;
  CardContent(this.content, this.list);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(vertical: 25, horizontal: 15),
      decoration: BoxDecoration(
        //color: Colors.lightBlue[50].withOpacity(0.9),
        borderRadius: BorderRadius.all(
          Radius.circular(SizeConfig.widthMultiplier * 3),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            content,
            style: TextStyle(
              fontWeight: FontWeight.w800,
              fontSize: SizeConfig.textMultiplier * 2.2,
              color: CustomColor.colorTextDeepBlue,
            ),
          ),
          SpacingBox(
            height: 2,
          ),
          CarouselSlider(
            options: CarouselOptions(
              height: SizeConfig.heightMultiplier * 9,
              viewportFraction: 0.6,
              autoPlay: false,
              enlargeCenterPage: true,
              enableInfiniteScroll: true,
            ),
            items: _buildListSchedule(list),
          ),
        ],
      ),
    );
  }
}

_buildListSchedule(List<dynamic> list) {
  return list
      .map<Widget>(
        (item) => Builder(builder: (context) {
          return Card(
            color: Colors.lightBlue[50].withOpacity(0.9),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("${item["startTime"]} - ${item["endTime"]}"),
                ],
              ),
            ),
          );
        }),
      )
      .toList();
}
