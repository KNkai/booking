
import 'package:flutter/material.dart';

import 'bookingSchedule_srv.dart';

class BookingScheduleRepo {
  BookingScheduleSrv _bookingScheduleSrv;
  BookingScheduleRepo({@required BookingScheduleSrv bookingScheduleSrv}) : _bookingScheduleSrv = bookingScheduleSrv;

  Future<dynamic> getTimeFrameByPackageId(String id) async {
    dynamic res = await _bookingScheduleSrv.getList(filter: "{ packageId: \"$id\"}");
    return res;
  }
}
