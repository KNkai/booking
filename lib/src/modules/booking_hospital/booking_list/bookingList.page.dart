import 'dart:ui';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/base_config/src/utils/asset.dart';
import 'package:super_init_app/base_widget/base_widget.dart';
import 'package:provider/provider.dart';
import 'package:super_init_app/routes.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_instance.dart';

import 'bookingList_bloc.dart';
import 'bookingList_repo.dart';
import 'bookingList_srv.dart';
import 'event/getBookingList_event.dart';

class BookingList extends StatelessWidget {
  BookingList();
  @override
  Widget build(BuildContext context) {
    return BasePageContainer(
      title: 'Trang danh sach booking',
      di: [
        Provider.value(value: BookingListSrv()),
        ProxyProvider<BookingListSrv, BookingListRepo>(
          update: (context, bookingListSrv, previous) =>
              BookingListRepo(bookingListSrv: bookingListSrv),
        )
      ],
      bloc: [],
      child: BookingListProvider(),
    );
  }
}

class BookingListProvider extends StatelessWidget {
  BookingListProvider();
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => BookingListBloc(bookingListRepo: Provider.of(context)),
        child: BookingListBody());
  }
}

class BookingListBody extends StatefulWidget {
  BookingListBody();

  @override
  _BookingListBodyState createState() => _BookingListBodyState();
}

class _BookingListBodyState extends State<BookingListBody> with BlocCreator {
  BookingListBloc bookingListBloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (bookingListBloc == null) {
      bookingListBloc = createBloc<BookingListBloc>();
      bookingListBloc.add(GetBookingListEvent());
    }
  }

  handleEvent(BaseEvent event) {
    if (event is LoadingFailEvent) {
      return;
    }
    if (event is LoadingSuccessEvent) {
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return LoadingTask(
      bloc: bookingListBloc,
      child: BlocListener<BookingListBloc>(
        listener: handleEvent,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            elevation: 0,
            leading: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Padding(
                  padding: EdgeInsets.all(10.0),
                  child: Icon(
                    Icons.navigate_before,
                    color: Colors.black.withOpacity(0.7),
                    size: 30,
                  )),
            ),
          ),
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(Assets.bg_listbook),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                top: 90,
                child: Padding(
                  padding: const EdgeInsets.only(left: 25.0, bottom: 15),
                  child: Text(
                    'Các gói khám bệnh',
                    style: TextStyle(
                      fontWeight: FontWeight.w800,
                      fontSize: SizeConfig.textMultiplier * 3.5,
                      color: CustomColor.colorTextDeepBlue,
                    ),
                  ),
                ),
              ),
              bookingListBloc.packageValue != null
                  ? Padding(
                      padding: EdgeInsets.only(top: 120),
                      child: RefreshIndicator(
                          onRefresh: () {
                            bookingListBloc.event.add(GetBookingListEvent());
                            return Future.delayed(Duration(seconds: 1));
                          },
                          child: Container(
                            height: MediaQuery.of(context).size.height,
                            child: ListView.builder(
                              padding: EdgeInsets.only(top: 10),
                              shrinkWrap: true,
                              physics: const AlwaysScrollableScrollPhysics(),
                              itemCount: bookingListBloc.packageValue.length,
                              itemBuilder: (context, index) => CardPackage(
                                bookingListBloc.packageValue[index],
                              ),
                            ),
                          )))
                  : SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }
}

class CardPackage extends StatelessWidget {
  final dynamic package;
  CardPackage(this.package);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 7),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.85),
        borderRadius: BorderRadius.all(
          Radius.circular(SizeConfig.widthMultiplier * 2),
        ),
      ),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          splashColor: Colors.lightBlue[100],
          onTap: () {
            BookingInstance.instance.selectedPackage = package;
            navigatorKey.currentState
                .pushNamed(RoutePaths.bookingInfo, arguments: package["_id"]);
          },
          child: Row(children: [
            Padding(
              padding: EdgeInsets.all(14),
              child: Material(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(
                  Radius.circular(80),
                ),
                elevation: 3,
                child: Container(
                  width: 75,
                  height: 75,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(80),
                      ),
                      gradient: LinearGradient(
                          colors: [Colors.blue[200], Colors.lightBlue[100]])),
                  child: Center(
                      child: SizedBox(
                    height: 55,
                    width: 55,
                    child: Image.asset('assets/images/medical.png'),
                  )),
                ),
              ),
            ),
            SpacingBox(width: 0),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    package["name"],
                    style: TextStyle(
                        letterSpacing: 0.1,
                        fontWeight: FontWeight.w800,
                        fontSize: SizeConfig.textMultiplier * 2.2,
                        color: CustomColor.colorTextDeepBlue),
                  ),
                  SpacingBox(height: 1),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 3),
                    decoration: BoxDecoration(
                      color: Colors.blueAccent.withOpacity(0.1),
                      borderRadius: BorderRadius.all(
                        Radius.circular(SizeConfig.widthMultiplier * 7),
                      ),
                    ),
                    child: Text(
                      Formart.toVNDCurency2(package["price"]),
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: SizeConfig.textMultiplier * 2.3,
                          color: HexColor('557FEA')),
                    ),
                  ),
                ],
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
