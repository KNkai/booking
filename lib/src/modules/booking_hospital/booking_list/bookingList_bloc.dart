import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_list/event/getBookingList_event.dart';

import 'bookingList_repo.dart';

class BookingListBloc extends BaseBloc with ChangeNotifier {
  BookingListBloc({@required BookingListRepo bookingListRepo}) {
    _bookingListRepo = bookingListRepo;
  }

  BookingListRepo _bookingListRepo;

  final _package$ = BehaviorSubject<dynamic>();
  Stream<dynamic> get packageStream => _package$.stream;
  Sink<dynamic> get packageSink => _package$.sink;
  dynamic get packageValue => _package$.value;

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case GetBookingListEvent:
        handleGetBookingListEvent(event);
        return;
      default:
    }
  }

  handleGetBookingListEvent(GetBookingListEvent event) async {
    loadingSink.add(true);
    final package = await _bookingListRepo.getAllPackage();
    packageSink.add(package["data"]);
    loadingSink.add(false);
    notifyListeners();
  }

  @override
  void dispose() {
    _package$.close();
    super.dispose();
  }
}
