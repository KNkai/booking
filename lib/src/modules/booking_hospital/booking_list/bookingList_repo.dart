
import 'package:flutter/material.dart';

import 'bookingList_srv.dart';

class BookingListRepo {
  BookingListSrv _bookingListSrv;
  BookingListRepo({@required BookingListSrv bookingListSrv}) : _bookingListSrv = bookingListSrv;

  Future<dynamic> getAllPackage() async {
    dynamic res = await _bookingListSrv.getList();
    return res;
  }
}
