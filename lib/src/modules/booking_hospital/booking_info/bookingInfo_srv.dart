




import 'package:super_init_app/base_config/src/base/base_service.dart';

class BookingInfoSrv extends BaseService {
  BookingInfoSrv() : super(module: 'Package', fragment: ''' 
_id: ID
createdAt: DateTime
updatedAt: DateTime
code: String
name: String
desc: String
content: String
price: Int
thumbnail: String
images: [String]
  ''');
}