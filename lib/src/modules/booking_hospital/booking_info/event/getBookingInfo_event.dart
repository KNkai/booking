
import 'package:super_init_app/base_config/base_config.dart';

class GetBookingInfoEvent extends BaseEvent {
  String id;
  GetBookingInfoEvent(this.id);
}
