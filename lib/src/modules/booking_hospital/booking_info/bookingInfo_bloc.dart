import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';

import 'bookingInfo_repo.dart';
import 'event/getBookingInfo_event.dart';


class BookingInfoBloc extends BaseBloc with ChangeNotifier {
  BookingInfoBloc({@required BookingInfoRepo bookingInfoRepo}) {
    _bookingInfoRepo = bookingInfoRepo;
  }

  BookingInfoRepo _bookingInfoRepo;

  final _package$ = BehaviorSubject<dynamic>();
  Stream<dynamic> get packageStream => _package$.stream;
  Sink<dynamic> get packageSink => _package$.sink;
  dynamic get packageValue => _package$.value;

  @override
  void dispatchEvent(BaseEvent event) {
    switch (event.runtimeType) {
      case GetBookingInfoEvent:
        handleGetBookingInfoEvent(event);
        return;
      default:
    }
  }

  handleGetBookingInfoEvent(GetBookingInfoEvent event) async {
    loadingSink.add(true);
    final package = await _bookingInfoRepo.getPackageInfo(event.id);
    packageSink.add(package);
    loadingSink.add(false);
    notifyListeners();
  }

  @override
  void dispose() {
    _package$.close();
    super.dispose();
  }
}
