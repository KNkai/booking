import 'dart:ui';

import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/base_config/src/utils/asset.dart';
import 'package:super_init_app/base_widget/base_widget.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:super_init_app/routes.dart';
import 'package:super_init_app/src/modules/booking_hospital/booking_instance.dart';
import 'bookingInfo_bloc.dart';
import 'bookingInfo_repo.dart';
import 'bookingInfo_srv.dart';
import 'event/getBookingInfo_event.dart';

class BookingInfo extends StatelessWidget {
  final String id;
  BookingInfo(this.id);
  @override
  Widget build(BuildContext context) {
    return BasePageContainer(
      title: 'Trang danh sach booking',
      di: [
        Provider.value(value: BookingInfoSrv()),
        ProxyProvider<BookingInfoSrv, BookingInfoRepo>(
          update: (context, bookingInfoSrv, previous) =>
              BookingInfoRepo(bookingInfoSrv: bookingInfoSrv),
        )
      ],
      bloc: [],
      child: BookingInfoProvider(id),
    );
  }
}

class BookingInfoProvider extends StatelessWidget {
  final String id;
  BookingInfoProvider(this.id);
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
        create: (_) => BookingInfoBloc(bookingInfoRepo: Provider.of(context)),
        child: BookingInfoBody(id));
  }
}

class BookingInfoBody extends StatefulWidget {
  final String id;
  BookingInfoBody(this.id);

  @override
  _BookingInfoBodyState createState() => _BookingInfoBodyState();
}

class _BookingInfoBodyState extends State<BookingInfoBody> with BlocCreator {
  BookingInfoBloc bookingInfoBloc;
  ScrollController _scrollController = ScrollController();
  bool isShowBottomCard = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (bookingInfoBloc == null) {
      bookingInfoBloc = createBloc<BookingInfoBloc>();
      bookingInfoBloc.add(GetBookingInfoEvent(widget.id));
    }
  }

  handleEvent(BaseEvent event) {
    if (event is LoadingFailEvent) {
      return;
    }
    if (event is LoadingSuccessEvent) {
      return;
    }
  }

  _onScroll(dynamic t) {
    if (t is ScrollEndNotification) {
      if (_scrollController.position.pixels >=
          MediaQuery.of(context).size.height / 4) {
        if (!isShowBottomCard) {
          setState(() {
            isShowBottomCard = true;
          });
        }
      } else {
        if (isShowBottomCard = true) {
          setState(() {
            isShowBottomCard = false;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return LoadingTask(
      bloc: bookingInfoBloc,
      child: BlocListener<BookingInfoBloc>(
        listener: handleEvent,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          appBar: AppBar(
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
                padding: EdgeInsets.all(10.0),
                child: Icon(
                  Icons.navigate_before,
                  color: Colors.black.withOpacity(0.7),
                  size: 30,
                )),
          ),
          actions: <Widget>[
            Center(
              child: IconButton(
                icon: Icon(EvaIcons.home, color: Colors.black.withOpacity(0.7)),
                onPressed: () {
                  navigatorKey.currentState.popUntil((route) => route.isFirst);
                },
              ),
            ),
          ],
        ),
          body: Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage(Assets.bg_doctor),
                    fit: BoxFit.contain,
                  ),
                ),
              ),
              bookingInfoBloc.packageValue != null
                  ? Padding(
                      padding: EdgeInsets.only(top: 80),
                      child: RefreshIndicator(
                        onRefresh: () {
                          bookingInfoBloc.event
                              .add(GetBookingInfoEvent(widget.id));
                          return Future.delayed(Duration(seconds: 1));
                        },
                        child: NotificationListener(
                          onNotification: (t) => _onScroll(t),
                          child: SingleChildScrollView(
                            controller: _scrollController,
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.widthMultiplier * 5),
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      bookingInfoBloc.packageValue["name"],
                                      style: TextStyle(
                                        fontWeight: FontWeight.w800,
                                        fontSize:
                                            SizeConfig.textMultiplier * 3.5,
                                        color: CustomColor.colorTextDeepBlue,
                                      ),
                                    ),
                                    SpacingBox(
                                      height: 2,
                                    ),
                                    CardPriceAction(
                                        bookingInfoBloc.packageValue["price"],
                                        widget.id),
                                    SpacingBox(height: 40),
                                    Container(
                                      padding: EdgeInsets.symmetric(
                                          vertical:
                                              SizeConfig.widthMultiplier * 5),
                                      color: Colors.white70,
                                      child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "Mô tả",
                                              style: TextStyle(
                                                fontWeight: FontWeight.w800,
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        3.1,
                                                color: CustomColor
                                                    .colorTextDeepBlue,
                                              ),
                                            ),
                                            SpacingBox(height: 3),
                                            Text(
                                              bookingInfoBloc
                                                  .packageValue["desc"],
                                              style: TextStyle(
                                                fontWeight: FontWeight.w300,
                                                fontSize:
                                                    SizeConfig.textMultiplier *
                                                        2,
                                                color: Colors.black87,
                                              ),
                                            ),
                                            SpacingBox(height: 3),
                                            CardContent(
                                              bookingInfoBloc
                                                  .packageValue["content"],
                                            ),
                                            SpacingBox(height: 3),
                                            CarouselSlider(
                                              options: CarouselOptions(
                                                height: SizeConfig
                                                        .heightMultiplier *
                                                    35,
                                                viewportFraction: 1,
                                                autoPlay: true,
                                                aspectRatio: 2.0,
                                                enlargeCenterPage: true,
                                                enableInfiniteScroll: true,
                                              ),
                                              items: _buildListImage(
                                                  bookingInfoBloc
                                                      .packageValue["images"]),
                                            ),
                                            SpacingBox(height: 10),
                                          ]),
                                    ),
                                  ]),
                            ),
                          ),
                        ),
                      ),
                    )
                  : SizedBox.shrink(),
              isShowBottomCard
                  ? Positioned(
                      bottom: 0,
                      child: Container(
                        color: Colors.white,
                        padding: EdgeInsets.all(15),
                        width: MediaQuery.of(context).size.width,
                        child: CardPriceAction(
                            bookingInfoBloc.packageValue["price"], widget.id),
                      ))
                  : SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }
}

class CardPriceAction extends StatelessWidget {
  final int price;
  final String id;
  CardPriceAction(this.price, this.id);
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        SpacingBox(
          width: 2,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'Giá gốc ' + Formart.toVNDCurency2(price),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.textMultiplier * 1.5,
                  color: Colors.black87),
            ),
            Text(
              Formart.toVNDCurency2(price),
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: SizeConfig.textMultiplier * 2.6,
                  color: HexColor('557FEA')),
            ),
            SpacingBox(
              width: 2,
            ),
          ],
        ),
        SpacingBox(width: 3),
        Expanded(
          child: Container(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Material(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(SizeConfig.widthMultiplier * 7),
                      bottomLeft:
                          Radius.circular(SizeConfig.widthMultiplier * 7),
                    ),
                    color: CustomColor.lightGreen,
                    child: InkWell(
                      borderRadius: BorderRadius.only(
                        topLeft:
                            Radius.circular(SizeConfig.widthMultiplier * 7),
                        bottomLeft:
                            Radius.circular(SizeConfig.widthMultiplier * 7),
                      ),
                      splashColor: Colors.blue,
                      onTap: () {
                        BookingInstance.instance.selectedDoctor = null; //remove selected doc
                        navigatorKey.currentState.pushNamed(
                            RoutePaths.bookingSchedule,
                            arguments: id);
                      },
                      child: Container(
                        height: SizeConfig.heightMultiplier * 6,
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            "Chọn ngày khám",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: SizeConfig.textMultiplier * 2.3,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SpacingBox(width: 0.3),
                Expanded(
                  child: Material(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(SizeConfig.widthMultiplier * 7),
                      bottomRight:
                          Radius.circular(SizeConfig.widthMultiplier * 7),
                    ),
                    color: CustomColor.lightGreen,
                    child: InkWell(
                      splashColor: Colors.blue,
                      borderRadius: BorderRadius.only(
                        topRight:
                            Radius.circular(SizeConfig.widthMultiplier * 7),
                        bottomRight:
                            Radius.circular(SizeConfig.widthMultiplier * 7),
                      ),
                      onTap: () {
                        BookingInstance.instance.selectedTime= null; //remove selected date
                        navigatorKey.currentState
                            .pushNamed(RoutePaths.bookingDoctorList);
                      },
                      child: Container(
                        height: SizeConfig.heightMultiplier * 6,
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                        child: FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Text(
                            "  Chọn bác sĩ     ",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: SizeConfig.textMultiplier * 2.3,
                                color: Colors.white),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class CardContent extends StatelessWidget {
  final String content;
  CardContent(this.content);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      padding: EdgeInsets.symmetric(vertical: 25, horizontal: 15),
      decoration: BoxDecoration(
        color: Colors.lightBlue[50].withOpacity(0.9),
        borderRadius: BorderRadius.all(
          Radius.circular(SizeConfig.widthMultiplier * 3),
        ),
      ),
      child: Column(
        children: <Widget>[
          SizedBox(
              height: SizeConfig.widthMultiplier * 20,
              child: Image.asset(Assets.medical2)),
          SpacingBox(
            height: 3,
          ),
          Text(
            content,
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: SizeConfig.textMultiplier * 2,
              color: Colors.black87,
            ),
          ),
        ],
      ),
    );
  }
}

_buildListImage(List<dynamic> list) {
  return list
      .map<Widget>(
        (item) => Builder(builder: (context) {
          return ClipRRect(
            borderRadius: BorderRadius.circular(5.0),
            child: CachedNetworkImage(
              imageUrl: item,
              placeholder: (context, url) => Center(
                child: SpinKitThreeBounce(
                  size: SizeConfig.widthMultiplier * 10,
                  color: CustomColor.main,
                  duration: Duration(milliseconds: 800),
                ),
              ),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          );
        }),
      )
      .toList();
}
