
import 'package:flutter/material.dart';

import 'bookingInfo_srv.dart';

class BookingInfoRepo {
  BookingInfoSrv _bookingInfoSrv;
  BookingInfoRepo({@required BookingInfoSrv bookingInfoSrv}) : _bookingInfoSrv = bookingInfoSrv;

  Future<dynamic> getPackageInfo(String id) async {
    dynamic res = await _bookingInfoSrv.getItem(id);
    return res;
  }
}
