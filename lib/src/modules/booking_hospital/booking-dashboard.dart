import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:super_init_app/base_config/base_config.dart';
import 'package:super_init_app/src/modules/booking_hospital/home/home_page.dart';
import 'package:super_init_app/src/modules/booking_hospital/notification/notificattion_page.dart';
import 'package:super_init_app/src/modules/booking_hospital/profile/profile_page.dart';
import 'package:super_init_app/src/modules/booking_hospital/support/support_page.dart';

class BookingDashboard extends StatefulWidget {
  BookingDashboard();

  @override
  _BookingDashboardState createState() => _BookingDashboardState();
}

class _BookingDashboardState extends State<BookingDashboard> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    SupportPage(),
    NotificationPage(),
    ProfilePage(),
  ];

  List<BottomNavigationBarItem> buildBottomNavBarItems() {
    return [
      BottomNavigationBarItem(
        icon: Icon(EvaIcons.home),
        title: Text('Trang chủ'),
      ),
      BottomNavigationBarItem(
        icon: Icon(EvaIcons.messageCircleOutline),
        title: Text('Trò chuyện'),
      ),
      BottomNavigationBarItem(
        icon: Icon(EvaIcons.bellOutline),
        title: Text('Thông báo'),
      ),
      BottomNavigationBarItem(
        icon: Icon(EvaIcons.personOutline),
        title: Text('Người dùng'),
      ),
    ];
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(
        index: _selectedIndex,
        children: _widgetOptions,
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: CustomColor.main,
        unselectedItemColor: Colors.grey,
        type: BottomNavigationBarType.shifting,
        items: buildBottomNavBarItems(),
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
      ),
    );
  }
}
