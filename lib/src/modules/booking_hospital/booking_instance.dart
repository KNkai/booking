class BookingInstance {

  BookingInstance._privateConstructor();

  static final BookingInstance _instance = BookingInstance._privateConstructor();

  static BookingInstance get instance => _instance;

  clearData() {
    selectedTime = null;
    selectedDoctor = null;
    selectedPackage = null;
  }

  dynamic selectedTime;
  dynamic selectedDoctor;
  dynamic selectedPackage;
}