import 'dart:io';
import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';
import 'package:super_init_app/base_config/src/base/base_service.dart';

class UserSrv extends BaseService {
  UserSrv()  : super(module: 'Agency', fragment: ''' 
  _id: ID
  uid: String
  name: String
  address: String
  phone: String
  email: String
  status: String
  long: Float
  lat: Float
  createdAt: DateTime
  updatedAt: DateTime
  ''');

  unregisterDevice() {}

  registerDeivce(String deviceFbToken) {}

  Future<String> _getUniqueDeviceId() {
    final infoPlugin = DeviceInfoPlugin();
    if (Platform.isIOS) {
      return infoPlugin.iosInfo.then((info) {
        return info.identifierForVendor;
      });
    } else if (Platform.isAndroid) {
      return infoPlugin.androidInfo.then((info) {
        return info.androidId;
      });
    }

    return null;
  }

  Future<Response> loginByPhonePin(phone, pin) {}

  Future<Response> loginByUserPassword(user, password) {}

  Future<bool> checkEmailExistFirebase(String email) {}

  Future<dynamic> loginEmailPasswordFirebase(String idToken) {
    
  }

  Future<Response> registerByEmailPasswordFirebase(user, password) {}

  Future<Response> registerByUserPassword(user, password) {}

  Future<Response> changePasswordByOtp(String pass, String token) {}

  Future<Response> checkExistPhone(String phone) {}
}
