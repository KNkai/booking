import 'dart:async';

import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/base_config.dart';

import 'user_srv.dart';

class UserRepo {
  UserSrv _userSrv = UserSrv();
  final FirebaseService _firebaseService;

  UserRepo({@required UserSrv userSrv, FirebaseService firebaseService})
      : _userSrv = userSrv,
        _firebaseService = firebaseService;

  registerUserDevice() {
    _firebaseService?.getDeviceToken()?.then((fbDeviceToken) {
      _userSrv.registerDeivce(fbDeviceToken);
    });
  }

  unregisterUserDevice() {
    _userSrv.unregisterDevice();
  }

  Future<String> loginByPhonePin(String phone, String pin) async {
    final c = Completer<String>();
    try {
      final res = await _userSrv.loginByPhonePin(phone, pin);
      final userToken = res.data['results']['object']['token'];
      final userId = res.data['results']['object']['agency']['_id'];

      await SPref.instance.set(CustomString.KEY_TOKEN, userToken);
      await SPref.instance.set(CustomString.KEY_ID, userId);
      c.complete(userToken);
    } on DioError catch (e) {
      final errorData = ErrorData.fromJson(e.response.data);
      final mess = errorData.message;
      c.completeError('Đăng nhập thất bại: $mess');
    } catch (e) {
      c.completeError(e);
    }

    return c.future;
  }

  Future<String> loginByUserPassword(String user, String password) async {
    final c = Completer<String>();
    try {
      final res = await _userSrv.loginByUserPassword(user, password);
      final userToken = 'token';
      final userId = 'id';
      await SPref.instance.set(CustomString.KEY_TOKEN, userToken);
      await SPref.instance.set(CustomString.KEY_ID, userId);
      c.complete(userToken.toString());
    } on DioError catch (e) {
      final errorData = ErrorData.fromJson(e.response.data);
      final mess = errorData.message;
      c.completeError('Đăng nhập thất bại: $mess');
    } catch (e) {
      c.completeError(e);
    }

    return c.future;
  }

  Future<String> loginByEmailPasswordFirebase(
      String email, String password) async {
    AuthResult authResult;
    try {
      authResult = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
    } catch (e) {
      String err = Formart.formatErrFirebaseLoginToString(e.code);
      throw Exception(err);
    }
    if (authResult.user == null) {
      throw Exception('Đăng nhập thất bại. Người dùng không tồn tại');
    } else {
      try {
        final firebaseToken = await authResult.user.getIdToken();
        final token = firebaseToken.token;
        final uid = authResult.user.uid;
        final input = 'token : "FB|$uid|$token" ';
        final res = await _userSrv.mutate('loginAgencyFirebase', input,
            fragment:
                'agency { _id uid name address phone email status long lat createdAt updatedAt } token');
        final data = res["loginAgencyFirebase"];
        print('FB|$uid|$token');
        await SPref.instance.set(CustomString.KEY_TOKEN, data["token"]);
        print('token: ${data["token"]}');
        await SPref.instance.set(CustomString.KEY_ID, data["agency"]["id"]);
        return data["token"];
      } catch (e) {
        throw Exception(e.toString());
      }
    }
  }

  Future<String> loginByOtpFirebase(AuthResult authResult) async {
    try {
      final firebaseToken = await authResult.user.getIdToken();
      final token = firebaseToken.token;
      final uid = authResult.user.uid;
      final input = 'token : "FB|$uid|$token" ';
      final res = await _userSrv.mutate('loginAgencyFirebase', input,
          fragment:
              'agency { _id uid name address phone email status long lat createdAt updatedAt } token');
      final data = res["loginAgencyFirebase"];
      print('FB|$uid|$token');
      await SPref.instance.set(CustomString.KEY_TOKEN, data["token"]);
      print('token: ${data["token"]}');
      await SPref.instance.set(CustomString.KEY_ID, data["agency"]["id"]);
      return data["token"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<String> changePassByOtp(String pass, String token) async {
    try {
      //final token = firebaseToken.token;
      //final uid = authResult.user.uid;
      final uid = "tATa0D0OdrP0CmlXAUbFfNnLtFj1";
      final token =
          "eyJhbGciOiJSUzI1NiIsImtpZCI6Ijc0Mzg3ZGUyMDUxMWNkNDgzYTIwZDIyOGQ5OTI4ZTU0YjNlZTBlMDgiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vdG9tLWdpb25nIiwiYXVkIjoidG9tLWdpb25nIiwiYXV0aF90aW1lIjoxNTkxMjU2NDQ4LCJ1c2VyX2lkIjoidEFUYTBEME9kclAwQ21sWEFVYkZmTm5MdEZqMSIsInN1YiI6InRBVGEwRDBPZHJQMENtbFhBVWJGZk5uTHRGajEiLCJpYXQiOjE1OTEyNTY0NDgsImV4cCI6MTU5MTI2MDA0OCwiZW1haWwiOiJhZ2VuY3lAZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbImFnZW5jeUBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.nn0cmCcHUZ4DY-zw1gnvn34fAAU9XfB7gfTiZLGkYd-_MEohohnRg80CIwBhROBuprchbI3D_rvwwRxWechaW0SEy709I3xlugCQxibvC6e2RkfK7dgJEUWOa2vfOKT2yxXDyZAkrl7HSDL8kHUBMvgfw9bLixSieS9VIw9nE0vM_0LUqazJazzBDoufY1zcxdnbdoIQfDDY9YJ-aahjlSlo5tKU35HE--LJZJE95Gysz4oCkRICOkxibOXI4fkSA3RYjpg035Xb0pEyWtO1N21chOFFaOvKWTdolc198zTj_UNTVswwapl-zeSzj8MXI34xZjMsp4Y6fwFjG0o_sw";
      final input = 'token : "FB|$uid|$token" , pin "$pass"';
      final res = await _userSrv.mutate('updatePinByFirebaseToken', input,
          fragment:
              ' _id ');
      final data = res["updatePinByFirebaseToken"];
      return data["_id"];
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  Future<bool> checkExistPhone(String phone) async {
    // var c = Completer<bool>();
    // try {
    //   final res = await _userSrv.checkExistPhone(phone);
    //   int flag = res.data['results']['object']['exist'];
    //   c.complete(flag == 1 ? true : false);
    // } on DioError catch (e) {
    //   if (e.response == null) {
    //     c.completeError(ErrorData.fromData(
    //         'Có lỗi xảy ra:Vui lòng kiểm tra kết nối mạng!'));
    //   } else {
    //     final errorData = ErrorData.fromJson(e.response.data);
    //     c.completeError(
    //         ErrorData.fromData('Không có dữ liệu! ${errorData.message}'));
    //   }
    // } catch (e) {
    //   c.completeError(ErrorData.fromData('Có lỗi xảy ra'));
    // }
    // return c.future;
    return true;
  }

  Future<bool> registerByUserNamePassword(String user, String password) async {
    var c = Completer<bool>();
    try {
      final res = await _userSrv.registerByUserPassword(user, password);
      int flag = res.data['results']['object']['exist'];
      c.complete(flag == 1 ? true : false);
    } on DioError catch (e) {
      if (e.response == null) {
        c.completeError(ErrorData.fromData(
            'Có lỗi xảy ra:Vui lòng kiểm tra kết nối mạng!'));
      } else {
        final errorData = ErrorData.fromJson(e.response.data);
        c.completeError(
            ErrorData.fromData('Không có dữ liệu! ${errorData.message}'));
      }
    } catch (e) {
      c.completeError(ErrorData.fromData('Có lỗi xảy ra'));
    }
    return c.future;
  }

  Future<String> registerByEmailFirebase(String email, String password) async {
    final c = Completer<String>();
    try {
      c.complete();
    } on DioError catch (e) {
      final errorData = ErrorData.fromJson(e.response.data);
      final mess = errorData.message;
      c.completeError('Đăng nhập thất bại: $mess');
    } catch (e) {
      c.completeError(e);
    }

    return c.future;
  }

  Future<bool> resetPassword(String email) async {
    bool flag = true;
    await FirebaseAuth.instance
        .sendPasswordResetEmail(email: email)
        .catchError((onError) {
      print(onError.toString());
      if (onError.code == 'ERROR_USER_NOT_FOUND') {}
      flag = false;
    });
    return flag;
  }
}
