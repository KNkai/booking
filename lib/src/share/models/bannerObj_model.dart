import 'package:super_init_app/src/share/models/banner_model.dart';

import 'paginationModel.dart';

class BannerObjectModel {
  Pagination pagination;
  List<BannerModel> banners;
  BannerObjectModel({this.pagination, this.banners});

  factory BannerObjectModel.fromJson(Map<String, dynamic> map) {
    return BannerObjectModel(
        banners: BannerModel.parseList(map['data']),
        pagination: Pagination.fromJson(map['pagination']));
  }
}
