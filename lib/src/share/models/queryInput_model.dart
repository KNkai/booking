import 'package:equatable/src/equatable_mixin.dart';

class QueryInput with EquatableMixin {
  QueryInput(
      {this.limit,
      this.skip,
      this.sort,
      this.filter,
      this.search,
      this.summary});

  factory QueryInput.fromJson(Map<String, dynamic> json) =>
      _$QueryInputFromJson(json);

  final int limit;

  final int skip;

  final dynamic sort;

  final dynamic filter;

  final dynamic search;

  final bool summary;

  @override
  List<Object> get props => [limit, skip, sort, filter, search, summary];
  Map<String, dynamic> toJson() => _$QueryInputToJson(this);
  QueryInput copyWith({
    int limit,
    int skip,
    dynamic sort,
    dynamic filter,
    String search,
    bool summary,
  }) {
    return QueryInput(
      limit: limit ?? this.limit,
      skip: skip ?? this.skip,
      sort: sort ?? this.sort,
      filter: filter ?? this.filter,
      search: search,
      summary: summary ?? this.summary,
    );
  }
}

QueryInput _$QueryInputFromJson(Map<String, dynamic> json) {
  return QueryInput(
    limit: json['limit'] as int,
    skip: json['skip'] as int,
    sort: json['sort'],
    filter: json['filter'],
    search: json['search'] as String,
    summary: json['summary'] as bool,
  );
}

Map<String, dynamic> _$QueryInputToJson(QueryInput instance) =>
    <String, dynamic>{
      'limit': instance.limit,
      'skip': instance.skip,
      'sort': instance.sort,
      'filter': instance.filter,
      'search': instance.search,
      'summary': instance.summary,
    };
