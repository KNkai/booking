class BannerModel {
  String id;
  String message;
  String picture;
  String body;
  String title;
  DateTime createdAt;
  DateTime updatedAt;
  DateTime startTime;
  DateTime endTime;
  int countSend;

  BannerModel(
      {this.id,
      this.message, // aka title
      this.body,
      this.picture,
      this.title,
      this.createdAt,
      this.updatedAt,
      this.startTime,
      this.endTime,
      this.countSend});

  static List<BannerModel> parseList(map) {
    var list = map as List;
    return list.map((item) => BannerModel.fromJson(item)).toList();
  }

  static List parseListToJson(List<BannerModel> list) {
    return list.map((f) => f.toJson()).toList();
  }

  factory BannerModel.fromJson(Map<String, dynamic> map) {
    return BannerModel(
        id: map['_id']??'',
        title: map['title']??'',
        message: map['message']??'',
        body: map['body']??'',
        picture: map['picture']??'',
        createdAt: DateTime.parse(map["createdAt"]),
        updatedAt: DateTime.parse(map["updatedAt"]),
        startTime: DateTime.parse(map["createdAt"]),
        endTime: DateTime.parse(map["createdAt"]),
        countSend: map['countSend']??0);
  }

  Map<String, dynamic> toJson() => {
        '_id': id,
        'message': message,
        'title': title,
        'body': body,
        'picture': picture,
        'createdAt': createdAt.toString(),
        'updatedAt': updatedAt.toString(),
      };
}
