class Pagination {
  int next;
  int prev;
  int total;

  Pagination({this.next, this.prev, this.total});

  factory Pagination.fromJson(Map<String, dynamic> map) {
    return Pagination(
      next: map['next'] ?? null,
      prev: map['prev'] ?? null,
      total: map['total'] ?? null,
    );
  }
}
