import 'package:super_init_app/base_config/base_config.dart';

class FilterEvent extends BaseEvent {
  final DateTime startDate;
  final DateTime endDate;
  final String status;

  FilterEvent({this.status, this.startDate, this.endDate});
}
