
// this event do nothing, do not implements dispatch on this event



import 'package:super_init_app/base_config/base_config.dart';

class LoadingFailEvent extends BaseEvent {
  String err;
  LoadingFailEvent(this.err);
}