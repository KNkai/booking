import 'package:flutter/material.dart';
import 'package:super_init_app/base_config/src/utils/color.dart';

class ProgressLinear extends StatelessWidget {
  final double progress;
  ProgressLinear(this.progress);
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Material(
        elevation: 1,
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: 4,
          child: Align(
            alignment: Alignment.centerLeft,
            child: Container(
              height: 4,
              width: progress * MediaQuery.of(context).size.width,
              color: CustomColor.main,
            ),
          ),
        ),
      ),
    );
  }
}
